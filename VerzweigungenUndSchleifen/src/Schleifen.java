import java.util.Scanner;

public class Schleifen {

	public static void fakultaet(Scanner s) {
		System.out.println("Bitte eine ganze Zahl zwischen 0 und 20 eingeben:");
		int n = s.nextInt();
		if (n <= 20) {
			int prod = 1;
			while (n > 1) {
				prod *= n;
				n--;
			}
			System.out.println(prod);
		} else {
			System.out.println("Zahl zu gro�.");
		}
	}

	public static void matrix(Scanner s) {
		int n = s.nextInt();
		int cnt = 0;

		while (cnt < 10) {
			if (cnt % n != 0) {
				char c = (char) cnt;
			} else {
				char c = '*';
			}
			cnt++;
		}
	}

	public static void ausgabeZaehlen(Scanner s) {
		System.out.println("Wie viele Zahlen sollen ausgegeben werden?");
		int n = s.nextInt();
		System.out.print("a) ");
		for (int i = 1; i < n; i++) {
			System.out.print(i + ", ");
		}
		System.out.println(n);
		System.out.print("b) ");
		for (int i = n; i > 1; i--) {
			System.out.print(i + ", ");
		}
		System.out.println(1);
	}

	public static void folgenAusgabe() {
		System.out.print("a) ");
		for (int i = 99; i > 9; i -= 3) {
			System.out.print(i + ", ");
		}
		System.out.println(9);

		System.out.print("b) ");
		for (int i = 1; i < 20; i++) {
			System.out.print((i * i) + ", ");
		}
		System.out.println(400);

		System.out.print("c) ");
		for (int i = 2; i < 102; i += 4) {
			System.out.print(i + ", ");
		}
		System.out.println(102);

		System.out.print("d) ");
		for (int i = 2; i < 32; i += 2) {
			System.out.print(i * i + ", ");
		}
		System.out.println(32 * 32);

		System.out.print("e) ");
		int res = 1;
		for (int i = 1; i < 15; i++) {
			res *= 2;
			System.out.print(res + ", ");
		}
		System.out.println(2 * res);
	}

	public static void ausgabeQuadrat(Scanner s) {
		System.out.println("Bitte Seitenlaenge eingeben: ");
		int seitenLaenge = s.nextInt();
		for (int i = 0; i < seitenLaenge; i++) {
			System.out.print("*   ");
			for (int j = 0; j < seitenLaenge - 2; j++) {
				int k = 0;
				if (i == 0 || i == seitenLaenge - 1) {
					System.out.print("*   ");
				} else {
					System.out.print("    ");
					if (k < seitenLaenge)
						System.out.print(" ");
				}
				k += 10;
			}
			System.out.println("*");
		}
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		// ausgabeZaehlen(scan); // AB Schleifen 1 A1
		// folgenAusgabe(); // AB Schleifen 1 A4
		// ausgabeQuadrat(scan); // AB Schleifen 1 A8
		// fakultaet(scan); // AB Schleifen 2 A2
		//matrix(scan);

		scan.close();
	}
}