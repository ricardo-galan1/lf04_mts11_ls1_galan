﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		// Scanner tastatur = new Scanner(System.in);

		/*
		 * anzahlFahrkarten ist vom Typ short, da es die üblichen Werte
		 * ausreichend abdeckt
		 */

		while (true) {

			double zuZahlenderBetrag = 0;
			double eingezahlterGesamtbetrag;

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n"
					+ "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");

		}
	}

	private static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Herzlich Willkommen!\n");
		System.out
				.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out
				.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		double zuZahlenderBetrag = 0;
		int anzahlFahrkarten = 0;
		while (true) {
			System.out.println("Ihre Wahl: ");
			anzahlFahrkarten = tastatur.nextShort();
			switch (anzahlFahrkarten) {
			case 1:
				zuZahlenderBetrag = 2.90;
				break;
			case 2:
				zuZahlenderBetrag = 8.60;
				break;
			case 3:
				zuZahlenderBetrag = 23.50;
				break;
			default:
				System.out.println(">>falsche Eingabe<<");
				break;
			}

			// System.out.print("Zu zahlender Betrag (EURO): ");
			// double zuZahlenderBetrag = tastatur.nextDouble();

			/*
			 * zuZahlenderBetrag wird mit anzahlFahrkarten mal genommen und in
			 * zuZahlenderBetrag gespeichert
			 */

			zuZahlenderBetrag *= anzahlFahrkarten;
			return zuZahlenderBetrag;
			
		}
	}

	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner scan = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f  " + "Euro\n",
					(zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = scan.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

	private static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag,
			double zuZahlenderBetrag) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f"
					+ " Euro\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.printf("%7s\n", "2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.printf("%7s\n", "1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.printf("%7s\n", "5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}
}