package AB_Ausgabeformatierung_2;

public class Temperaturtabelle {

	public static void main(String[] args) {
		
		System.out.printf("%-11s|%10s", "Fahrenheit", "Celsius\n");
		System.out.println("---------------------");
		System.out.printf("%-11.0f|%9.2f\n", -20f, -28.89f);
		System.out.printf("%-11.0f|%9.2f\n", -10f, -23.33f);
		System.out.printf("%+-11.0f|%9.2f\n", 0f, -17.78f);
		System.out.printf("%+-11.0f|%9.2f\n", 20f, -6.67f);
		System.out.printf("%+-11.0f|%9.2f", 30f, -1.11f);
		
	}
}
