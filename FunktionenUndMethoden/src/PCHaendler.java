import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("was moechten Sie bestellen?", myScanner);
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:", myScanner);

		double preis = liesDouble("Geben Sie den Nettopreis ein:", myScanner);

		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:", myScanner);

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
 
		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		myScanner.close();
	}
	
	public static String liesString(String text, Scanner scan){
		System.out.println(text);
		String ausgabe = scan.next();
		return ausgabe;
	}
	
	public static int liesInt(String text, Scanner scan){
		System.out.println(text);
		int ausgabe = scan.nextInt();
		return ausgabe;
	}
	
	public static double liesDouble(String text, Scanner scan){
		System.out.println(text);
		double ausgabe = scan.nextDouble();
		return ausgabe;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis){
		return (anzahl * nettopreis);
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst){
		return (nettogesamtpreis * (1 + mwst / 100));
	}
}
