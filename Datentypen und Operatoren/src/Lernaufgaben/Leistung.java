package Lernaufgaben;

import java.util.Scanner;

public class Leistung {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		double einzelleistung;
		double gesamtleistung;
		double gesamtstromstaerke;
		final double netzspannung = 230.0;
		final double maxStromstaerke = 16.0;
		int anzahlPCs;
		int anzahlStromkreise;
		
		
		System.out.print("\nLeistung eines PC-Arbeitsplatzes [in Watt]: ");
		einzelleistung = myScanner.nextDouble();
		System.out.print("Anzahl der PC-Arbeitspl�tze: ");
		anzahlPCs = myScanner.nextInt();
		
		
		/*	Erg�nzen Sie das Programm Leistung. Das Programm liest die Anzahl der PCs und die 
			Leistungsaufnahme eines PCs (in W, einschlie�lich Monitor) ein und soll daraus die Gesamtleistung 
			und die Gesamtstromst�rke und die erforderliche Anzahl an Stromkreisen berechnen.
			Die Versorgungsspannung betr�gt U = 230 V, die maximale Stromst�rke in einem Stromkreis 
			Imax = 16 A.
			Hinweis: Leistung = Spannung * Stromst�rke, Gesamtleistung = Summe der Einzelleistungswerte.
		 */
		
		// Berechnung der erforderlichen Stromst�rke und der
		// Anzahl der ben�tigten Stromkreise:
		
		
		gesamtleistung = einzelleistung*anzahlPCs;
		gesamtstromstaerke = gesamtleistung/netzspannung;
		anzahlStromkreise = (int) Math.ceil(gesamtstromstaerke/maxStromstaerke);
		
		System.out.println("Gesamtleistung: " + gesamtleistung);
		System.out.println("Gesamtstromst�rke: " + gesamtstromstaerke);
		System.out.println("Anzahl der Stromkreise: " + anzahlStromkreise);
		myScanner.close();
	}
}
